//
//  Profile.swift
//  NewsDemo
//
//  Created by Mikhail Kulichkov on 24/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import Foundation

struct Profile {
    
    // MARK: Language
    
    static var language: Language {
        return _language ?? .english
    }
    
    private static var _language: Language? = {
        if let languageRaw = Locale.current.languageCode?.lowercased() {
            return Language(rawValue: languageRaw)
        }
        return nil
    }()
    
    // MARK:  Country
    
    static var country: Country? {
        return _country
    }
    
    private static var _country: Country? = {
        if let countryRaw = Locale.current.regionCode?.lowercased() {
            return Country(rawValue: countryRaw)
        }
        return nil
    }()
    
}
