//
//  NewsAPIResponse.swift
//  NewsDemo
//
//  Created by Mikhail Kulichkov on 24/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import Foundation

protocol NewsAPIResponse: Codable {
    
    var status: NewsApiResponseStatus? { get set }
    var code: String? { get set }
    var message: String? { get set }
    
}


