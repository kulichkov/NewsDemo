//
//  SourcesResponse.swift
//  NewsDemo
//
//  Created by Mikhail Kulichkov on 17/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import Foundation

struct SourcesResponse: NewsAPIResponse, Equatable {
   
    var status: NewsApiResponseStatus?
    var code: String?
    var message: String?
    
    let sources: [Source]?
    
}
