//
//  Source.swift
//  NewsDemo
//
//  Created by Mikhail Kulichkov on 17/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import Foundation

struct Source: Codable, Equatable {
    
    let id: String?
    let name: String?
    let description: String?
    let url: String?
    let category: NewsCategory?
    let language: Language?
    let country: Country?
    
}
