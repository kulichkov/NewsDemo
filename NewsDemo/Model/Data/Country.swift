//
//  Country.swift
//  NewsDemo
//
//  Created by Mikhail Kulichkov on 23/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import Foundation

enum Country: String, Codable {
    
    case Argentina = "ar"
    case Australia = "au"
    case Austria = "at"
    case Belgium = "be"
    case Brazil = "br"
    case Bulgaria = "bg"
    case Canada = "ca"
    case China = "zh"
    case Colombia = "co"
    case Cuba = "cu"
    case CzechRepublic = "cz"
    case Egypt = "eg"
    case France = "fr"
    case Germany = "de"
    case Greece = "gr"
    case HongKong = "hk"
    case Hungary = "hu"
    case India = "in"
    case Indonesia = "id"
    case Ireland = "ie"
    case Israel = "is"
    case Italy = "it"
    case Japan = "jp"
    case Latvia = "lv"
    case Lithuania = "lt"
    case Malaysia = "my"
    case Mexico = "mx"
    case Morocco = "ma"
    case Netherlands = "nl"
    case NewZealand = "nz"
    case Nigeria = "ng"
    case Norway = "no"
    case Pakistan = "pk"
    case Philippines = "ph"
    case Poland = "pl"
    case Portugal = "pt"
    case Romania = "ro"
    case Russia = "ru"
    case SaudiArabia = "sa"
    case Serbia = "rs"
    case Singapore = "sg"
    case Slovakia = "sk"
    case Slovenia = "si"
    case SouthAfrica = "za"
    case SouthKorea = "kr"
    case Spain = "es"
    case Sweden = "se"
    case Switzerland = "ch"
    case Taiwan = "tw"
    case Thailand = "th"
    case Turkey = "tr"
    case UAE = "ae"
    case Ukraine = "ua"
    case UnitedKingdom = "gb"
    case UnitedStates = "us"
    case Venuzuela = "ve"
    
}
