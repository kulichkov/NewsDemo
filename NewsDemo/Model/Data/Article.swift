//
//  Article.swift
//  NewsDemo
//
//  Created by Mikhail Kulichkov on 17/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import Foundation
 
struct Article: Codable, Equatable {
    
    let source: Source?
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    
}
