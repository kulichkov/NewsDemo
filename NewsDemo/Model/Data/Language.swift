//
//  Language.swift
//  NewsDemo
//
//  Created by Mikhail Kulichkov on 23/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import Foundation

enum Language: String, Codable {
    
    case arabic = "ar"
    case german = "de"
    case english = "en"
    case spanish = "es"
    case french = "fr"
    case hebrew = "he"
    case italian = "it"
    case dutch = "nl"
    case norwegian = "no"
    case portuguese = "pt"
    case russian = "ru"
    case swedish = "se"
    case urdu = "ud"
    case chinese = "zh"
    
}
