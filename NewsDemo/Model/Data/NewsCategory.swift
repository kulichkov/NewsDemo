//
//  NewsCategory.swift
//  NewsDemo
//
//  Created by Mikhail Kulichkov on 24/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import Foundation

enum NewsCategory: String, Codable {
    
    case business, entertainment, general, health, science, sports, technology
    
}
