//
//  NewsApiRequestManager.swift
//  NewsDemo
//
//  Created by Mikhail Kulichkov on 17/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import Foundation

//Encode params to be web friendly and return a string. Reference ->http://stackoverflow.com/a/27724627/6091482
extension String {
    
    /// Percent escapes values to be added to a URL query as specified in RFC 3986
    ///
    /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "~".
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: Returns percent-escaped string.
    
    func stringByAddingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }

}

extension Dictionary {
    
    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    func stringFromHttpParameters() -> String {
        let parameterArray = self.compactMap { (key, value) -> String? in
            if let key = key as? String,
                let value = value as? String,
                let percentEscapedKey = key.stringByAddingPercentEncodingForURLQueryValue(),
                let percentEscapedValue = value.stringByAddingPercentEncodingForURLQueryValue() {
                return "\(percentEscapedKey)=\(percentEscapedValue)"
            } else {
                return nil
            }
        }
        
        return parameterArray.joined(separator: "&")
    }
}

enum NewsApiResponseStatus: String, Codable {
    case ok, error
}

class NewsAPIRequestManager {
    
    static let baseURLString: String = "https://newsapi.org"
    static let apiVersion: String = "v2"
    static let apiKey: String = "95092b3b71a549d98b1c49dc9b4db3f6"
    
    @discardableResult
    private static func startNewsAPIDataTask(methodName: String, parameters: [String: Any]?, completion: ((Data?, Error?) -> ())?) -> URLSessionTask? {
        var stringURL = "\(baseURLString)/\(apiVersion)/\(methodName)"
        let parameterString = parameters?.stringFromHttpParameters()
        
        if let parameterString = parameterString, !parameterString.isEmpty {
            stringURL += "?\(parameterString)"
        }
        
        let requestURL: URL = URL(string: stringURL)!
        
        print("Request URL: \(requestURL)")
        
        var request: URLRequest = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        request.addValue(apiKey, forHTTPHeaderField: "x-api-key")
        let dataTask: URLSessionDataTask = URLSession.shared.dataTask(with: request) { (data: Data?, urlResponse: URLResponse?, error: Error?) in
            completion?(data, error)
        }
        dataTask.resume()
        return dataTask
    }
    
    @discardableResult
    static func getSources(country: Country? = nil,
                           language: Language? = nil,
                           category: NewsCategory? = nil,
                           completion: ((SourcesResponse?, Error?) -> ())?) -> URLSessionTask? {
        
        var params: [String: Any] = [:]
        if let language = language {
            params["language"] = language.rawValue
        }
        if let category = category {
            params["category"] = category.rawValue
        }
        
        return startNewsAPIDataTask(methodName: "sources",
                                    parameters: params,
                                    completion: { (data: Data?, error: Error?) in
                                        var sourcesResponse: SourcesResponse?
                                        if let data = data {
                                            sourcesResponse = try? JSONDecoder().decode(SourcesResponse.self, from: data)
                                        }
                                        completion?(sourcesResponse, error) })
    }
    
    /**
     Creates and returns URLSessionTask of fetching top headlines
     
     - parameter country: You can't mix this param with the sources param.
     - parameter category: The category you want to get headlines for. You can't mix this param with the sources param.
     - parameter sources: News sources or blogs you want headlines from. You can't mix this param with the country or category params.
     - parameter keywords: Keywords or a phrase to search for.
     - parameter pageSize: The number of results to return per page (request). 20 is the default, 100 is the maximum.
     - parameter page: Use this to page through the results if the total results found is greater than the page size.
     - parameter completion: Completion closure with top headlines response object and error
    */
    
    @discardableResult
    static func getTopHeadlines(country: Country? = nil,
                                category: NewsCategory? = .general,
                                sources: [Source]? = nil,
                                keywords: String? = nil,
                                pageSize: Int? = nil,
                                page: Int? = nil,
                                completion: ((TopHeadlinesResponse?, Error?) -> ())?) -> URLSessionTask? {
        
        var params: [String: Any] = [:]
        
        if country != nil || category != nil {
            if let country = country {
                params["country"] = country.rawValue
            }
            if let category = category {
                params["category"] = category.rawValue
            }
        } else if let sources = sources, !sources.isEmpty {
            params["sources"] = sources.compactMap { $0.id }.joined(separator: ",")
        }
        
        if let keywords = keywords {
            params["q"] = keywords
        }
        if let pageSize = pageSize {
            params["pageSize"] = pageSize
        }
        if let page = page {
            params["page"] = page
        }
        
        return startNewsAPIDataTask(methodName: "top-headlines",
                                    parameters: params,
                                    completion: { (data: Data?, error: Error?) in
                                        var topHeadlinesResponse: TopHeadlinesResponse?
                                        if let data = data {
                                            topHeadlinesResponse = try? JSONDecoder().decode(TopHeadlinesResponse.self, from: data)
                                        }
                                        completion?(topHeadlinesResponse, error) })
        
    }
}
