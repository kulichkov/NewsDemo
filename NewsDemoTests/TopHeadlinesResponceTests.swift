//
//  TopHeadlinesResponceTests.swift
//  NewsDemoTests
//
//  Created by Mikhail Kulichkov on 24/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import XCTest
@testable import NewsDemo

class TopHeadlinesResponceTests: XCTestCase {
    
    var jsonEncoder: JSONEncoder!
    var jsonDecoder: JSONDecoder!
    
    override func setUp() {
        super.setUp()
        jsonEncoder = JSONEncoder()
        jsonDecoder = JSONDecoder()
    }
    
    override func tearDown() {
        jsonEncoder = nil
        jsonDecoder = nil
        super.tearDown()
    }
    
    
    func testGetTopHeadlinesResponse() {
        
        let topHeadlinesExpectation = expectation(description: "data is a ok, fields are ok")
        
        NewsAPIRequestManager.getTopHeadlines(country: .Russia,
                                              category: .technology,
                                              sources: nil,
                                              keywords: "iOS",
                                              pageSize: nil,
                                              page: nil,
                                              completion: { (topHeadlinesResponse: TopHeadlinesResponse?, error:Error?) in
                                                if let articles = topHeadlinesResponse?.articles {
                                                    topHeadlinesExpectation.fulfill()
                                                    for article in articles {
                                                        if article.source?.category != .technology || article.source?.country != .Russia {
                                                            XCTFail("Inconsistent fetch results")
                                                        }
                                                    }
                                                    // print(topHeadlinesResponse?.totalResults)
                                                    // print(articles)
                                                } else if let errorCode = topHeadlinesResponse?.code, let errorMessage = topHeadlinesResponse?.message {
                                                    print("errorCode: \(errorCode)\nerrorMessage: \(errorMessage)")
                                                } })
        
        wait(for: [topHeadlinesExpectation], timeout: 5)
    }
    
    func testGetSourcesResponseWith() {
        
        let sourcesExpectation = expectation(description: "data is a ok, fields are ok")
        
        NewsAPIRequestManager.getSources(country: .Brazil,
                                         language: .portuguese,
                                         category: .business) { (sourcesResponse: SourcesResponse?, error: Error?) in
                                            if let sources = sourcesResponse?.sources {
                                                sourcesExpectation.fulfill()
                                                for source in sources {
                                                    if source.category != .business || source.country != .Brazil || source.language != .portuguese {
                                                        XCTFail("Inconsistent fetch results")
                                                    }
                                                }
                                            } else if let error = error {
                                                print(error)
                                            } }
        
        wait(for: [sourcesExpectation], timeout: 5)
    }
    
}

