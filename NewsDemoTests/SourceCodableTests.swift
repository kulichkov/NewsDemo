//
//  SourceCodableTests.swift
//  NewsDemoTests
//
//  Created by Mikhail Kulichkov on 17/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import XCTest
@testable import NewsDemo

class SourceCodableTests: XCTestCase {
    
    var jsonEncoder: JSONEncoder!
    var jsonDecoder: JSONDecoder!
    
    override func setUp() {
        super.setUp()
        jsonEncoder = JSONEncoder()
        jsonDecoder = JSONDecoder()
    }
    
    override func tearDown() {
        jsonEncoder = nil
        jsonDecoder = nil
        super.tearDown()
    }
    
    func testSourceCodable() {
        let source: Source = Source(id: "testID",
                                    name: "testName",
                                    description: "testDescription",
                                    url: "testURL",
                                    category: .health,
                                    language: .dutch,
                                    country: .Netherlands)
        let encodedData = try? jsonEncoder.encode(source)
        XCTAssert(encodedData != nil)
        
        if let decodedSource = try? jsonDecoder.decode(Source.self, from: encodedData!) {
//            print(decodedSource)
            XCTAssert(decodedSource == source)
        } else {
            XCTFail("Error during decoding")
        }
        
    }
    
    func testSourcesCodable() {
        let source1: Source = Source(id: "testID1",
                                     name: "testName1",
                                     description: "testDescription1",
                                     url: "testURL1",
                                     category: .health,
                                     language: .dutch,
                                     country: .Netherlands)
        let source2: Source = Source(id: "testID2",
                                     name: "testName2",
                                     description: "testDescription2",
                                     url: "testURL2",
                                     category: .business,
                                     language: .english,
                                     country: .Russia)
        
        let encodedData = try? jsonEncoder.encode([source1, source2])
        
        XCTAssert(encodedData != nil)
        
        if let decodedSources = try? jsonDecoder.decode([Source].self, from: encodedData!) {
//            print(decodedSources)
            XCTAssert(decodedSources == [source1, source2])
        } else {
            XCTFail("Error during decoding")
        }
        
    }
    
}
