//
//  SourceTests.swift
//  NewsDemoTests
//
//  Created by Mikhail Kulichkov on 17/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import XCTest
@testable import NewsDemo

class SourceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSourcesParsing() {
        let sourcesExpectation = expectation(description: "Sources")
        
        NewsAPIRequestManager.getSources(completion: { (sourcesResponse: SourcesResponse?, error: Error?) in
            if let sources = sourcesResponse?.sources, sources.count > 0 {
                sourcesExpectation.fulfill()
            } else if let error = error {
                print(error)
            } })
        
        wait(for: [sourcesExpectation], timeout: 5)
    }
    
}
