//
//  SourcesResponseTests.swift
//  NewsDemoTests
//
//  Created by Mikhail Kulichkov on 17/06/2018.
//  Copyright © 2018 Mikhail Kulichkov. All rights reserved.
//

import XCTest
@testable import NewsDemo

class SourcesResponseTests: XCTestCase {
    
    var jsonEncoder: JSONEncoder!
    var jsonDecoder: JSONDecoder!
    
    override func setUp() {
        super.setUp()
        jsonEncoder = JSONEncoder()
        jsonDecoder = JSONDecoder()
    }
    
    override func tearDown() {
        jsonEncoder = nil
        jsonDecoder = nil
        super.tearDown()
    }
    
    func testSourcesResponseCodable() {
        let source1: Source = Source(id: "testID1",
                                     name: "testName1",
                                     description: "testDescription1",
                                     url: "testURL1",
                                     category: .health,
                                     language: .dutch,
                                     country: .Netherlands)
        let source2: Source = Source(id: "testID2",
                                     name: "testName2",
                                     description: "testDescription2",
                                     url: "testURL2",
                                     category: .business,
                                     language: .english,
                                     country: .Russia)
        
        let sourcesResponse: SourcesResponse = SourcesResponse(status: .ok, code: nil, message: nil, sources: [source1, source2])
        
//        (status: .ok, code: nil, message: nil, sources: [source1, source2])
        
        let encodedData = try? jsonEncoder.encode(sourcesResponse)
        XCTAssert(encodedData != nil)
        
        if let decodedSource = try? jsonDecoder.decode(SourcesResponse.self, from: encodedData!) {
//            print(decodedSource)
            XCTAssert(decodedSource == sourcesResponse)
        } else {
            XCTFail("Error during decoding")
        }
    }
    
    func testGetSourcesResponse() {
        
        let sourcesExpectation = expectation(description: "data is a ok, fields are ok")
        
        NewsAPIRequestManager.getSources(country: .Brazil,
                                         language: .portuguese,
                                         category: nil) { (sourcesResponse: SourcesResponse?, error: Error?) in
                                            if let sources = sourcesResponse?.sources {
                                                sourcesExpectation.fulfill()
                                                for source in sources {
//                                                    print(source)
                                                    if source.country != .Brazil || source.language != .portuguese {
                                                        XCTFail("Inconsistent fetch results")
                                                    }
                                                }
                                            } else if let error = error {
                                                print(error)
                                            } }
        
        wait(for: [sourcesExpectation], timeout: 5)
    }
    
    func testGetSourcesResponseWith() {
        
        let sourcesExpectation = expectation(description: "data is a ok, fields are ok")
        
        NewsAPIRequestManager.getSources(country: .Brazil,
                                         language: .portuguese,
                                         category: .business) { (sourcesResponse: SourcesResponse?, error: Error?) in
                                            if let sources = sourcesResponse?.sources {
                                                sourcesExpectation.fulfill()
                                                for source in sources {
                                                    if source.category != .business || source.country != .Brazil || source.language != .portuguese {
                                                        XCTFail("Inconsistent fetch results")
                                                    }
                                                }
                                            } else if let error = error {
                                                print(error)
                                            } }
        
        wait(for: [sourcesExpectation], timeout: 5)
    }
    
}
